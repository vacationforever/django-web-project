from django.db import models
from django.contrib.auth.models import User
import math


class Product(models.Model):
    title = models.CharField(verbose_name='Наименование товара', max_length=50)
    description = models.TextField(verbose_name='Описание товара')
    category = models.ManyToManyField("Category", verbose_name="Категории")
    price = models.PositiveIntegerField(verbose_name='Цена', default=0)
    article = models.IntegerField(verbose_name='Номер серии', default=0)
    image = models.ImageField(
        verbose_name="Фото товара", upload_to='img/products', default='')
    seller = models.ForeignKey(
        "Seller", verbose_name="Продавец", on_delete=models.CASCADE)

    def get_total_rating(self):
        marks = self.user_stars.count()
        ratings = Rating.objects.filter(product=self)
        total = 0
        arr = []
        for rating in ratings:
            total += rating.stars
        try:
            for i in range(math.floor(total/marks)):
                arr.append(i)
            return arr
        except ZeroDivisionError:
            return arr

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'


class Category(models.Model):
    title = models.CharField(verbose_name='Описание категории', max_length=50)
    description = models.TextField(verbose_name='Описание категории')
    image = models.ImageField(
        verbose_name='Фото категории', upload_to='img/categories', default='')
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Seller(models.Model):
    name = models.CharField(
        verbose_name='Наименование компании-продавца', max_length=50)
    description = models.TextField(verbose_name='Описание компании-продавца')
    contacts = models.TextField(verbose_name='Контакты')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Продавец'
        verbose_name_plural = 'Продавцы'


class Rating(models.Model):
    creator = models.ForeignKey(
        User, verbose_name='Оставитель отзыва', on_delete=models.CASCADE, default=0)
    product = models.ForeignKey(
        Product, verbose_name='Товар', related_name='user_stars', on_delete=models.CASCADE, default=0)
    title = models.CharField(
        verbose_name='Тема', max_length=50)
    text = models.TextField(verbose_name='Сообщение')
    stars = models.PositiveSmallIntegerField(
        verbose_name='рейтинг', default=0)

    def __str__(self):
        return f'{self.title} - {self.stars}'

    class Meta:
        unique_together = ('creator', 'product')
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'


class Callback(models.Model):
    phone = models.CharField(verbose_name="Номер телефона", max_length=50)
    name = models.CharField(verbose_name="Имя", max_length=50)
    done = models.BooleanField(verbose_name='Звонок выполнен', default=False)

    def __str__(self):
        return f'{self.name} - {self.done}'

    class Meta:
        verbose_name = 'Звонок'
        verbose_name_plural = 'Звонки'

STATUS_CHOICES = [
    ('Не выполнен', 'Не выполнен'),
    ('Выполнен', 'Выполнен'),
]

class Order(models.Model):
    user = models.ForeignKey(
        User, verbose_name='Заказчик', on_delete=models.CASCADE, default=0)
    products = models.ManyToManyField(Product, verbose_name='Товары')
    status = models.CharField(verbose_name='Выполнен?', choices=STATUS_CHOICES,default='Не выполнен', max_length=50)
    total = models.PositiveIntegerField(verbose_name='Итоговая сумма', default=0)

    def __str__(self):
        return f'{self.id}'

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


class Basket(models.Model):
    user = models.OneToOneField(User, verbose_name='Пользователь', on_delete=models.CASCADE)
    products = models.ManyToManyField(Product, verbose_name='Товары', blank=True, default=[])
    subtotal = models.PositiveIntegerField(verbose_name='Подстоимость', default=0)

    def __str__(self):
        return f'Корзина пользователя {self.user.username}'

    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзины'