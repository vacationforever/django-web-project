from django import forms

class CallbackForm(forms.Form):
    name = forms.CharField(required=True, label='Имя')
    phone = forms.CharField(required=True, label='Номер телефона')

mark = [
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
]

class RatingForm(forms.Form):
    title = forms.CharField(required=True, label='Тема')
    description = forms.CharField(widget=forms.Textarea(), required=True,label='Сообщение')
    stars = forms.ChoiceField(choices=mark,required=True,label='Рейтинг')